package apacheCommonsCSVLib.basics;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CreatingCSV1 {
    public static void main(String[] args) throws IOException {
        Map<String, String> AUTHOR_BOOK_MAP = new HashMap<>() {
            {
                put("Dan Simmons", "Hyperion");
                put("Douglas Adams", "The Hitchhiker's Guide to the Galaxy");
            }
        };

        FileWriter out = new FileWriter("src/main/resources/book_new2.csv");

        try (CSVPrinter printer = CSVFormat.DEFAULT
                .withHeader("author", "title").print(out)) {
            AUTHOR_BOOK_MAP.forEach(
                    (author, title) -> {
                        try {
                            System.out.println(author + title);
                            printer.printRecord(author, title);
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }
                    }
            );
        }
    }
}
