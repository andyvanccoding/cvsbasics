package apacheCommonsCSVLib.basics;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class ReadingCSV {

    public static void main(String[] args) throws IOException {
        Reader in = new FileReader("D:\\obelisk\\workingWithCSV\\src\\main\\resources\\book.csv");
        Iterable<CSVRecord> records = CSVFormat.DEFAULT
                .withFirstRecordAsHeader()
                .parse(in);
        for (CSVRecord record : records) {
            String author = record.get("author");
            String title = record.get("title");
            System.out.printf("\nThis is the author: %s and booktitle: %s", author, title);
        }
    }
}
