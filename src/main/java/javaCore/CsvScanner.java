package javaCore;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CsvScanner {
    static String COMMA_DELIMETER = ",";

    public static void main(String[] args) {
        List<List<String>> records = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File("D:\\obelisk\\workingWithCSV\\src\\main\\resources\\weather-2012-01-01.csv"));
            while (scanner.hasNextLine()) {
                records.add(getRecordFromLine(scanner.nextLine()));
            }
        } catch (IOException ioException) {
            System.out.println(ioException);
        }

        System.out.println(records.size());
        records.forEach(System.out::println);
    }

    private static List<String> getRecordFromLine(String nextLine) {
        List<String> values = new ArrayList<>();
        try (Scanner rowScanner = new Scanner(nextLine)) {
            rowScanner.useDelimiter(COMMA_DELIMETER);
            while (rowScanner.hasNext()) {
                values.add(rowScanner.next());
            }
        }
        return values;
    }
}
